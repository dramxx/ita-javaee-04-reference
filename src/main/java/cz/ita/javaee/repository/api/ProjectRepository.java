package cz.ita.javaee.repository.api;

import cz.ita.javaee.model.ProjectEntity;

public interface ProjectRepository extends EntityRepository<ProjectEntity> {

}