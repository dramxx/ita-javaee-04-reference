package cz.ita.javaee.repository.api;

import cz.ita.javaee.model.TmDeliveryEntity;

public interface TmDeliveryRepository extends EntityRepository<TmDeliveryEntity> {

}