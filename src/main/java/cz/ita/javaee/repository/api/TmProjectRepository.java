package cz.ita.javaee.repository.api;

import cz.ita.javaee.model.TmProjectEntity;

public interface TmProjectRepository extends EntityRepository<TmProjectEntity> {

}