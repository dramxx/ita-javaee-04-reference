package cz.ita.javaee.repository.api;

import cz.ita.javaee.model.CompanyEntity;

public interface CompanyRepository extends EntityRepository<CompanyEntity> {

}