package cz.ita.javaee.repository.api;

import cz.ita.javaee.model.TmOrderEntity;

public interface TmOrderRepository extends EntityRepository<TmOrderEntity> {

}