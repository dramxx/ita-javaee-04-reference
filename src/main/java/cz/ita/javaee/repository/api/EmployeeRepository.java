package cz.ita.javaee.repository.api;

import cz.ita.javaee.model.EmployeeEntity;

public interface EmployeeRepository extends EntityRepository<EmployeeEntity> {

}