package cz.ita.javaee.repository;

import cz.ita.javaee.model.AddressEntity;
import cz.ita.javaee.model.EmployeeEntity;
import cz.ita.javaee.repository.api.EmployeeRepository;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Repository
@Profile("inMemory")
public class EmployeeInMemoryRepository implements EmployeeRepository {

    private final List<EmployeeEntity> DATA = new ArrayList<>();

    @PostConstruct
    private void init() {

        EmployeeEntity employee1 = new EmployeeEntity();
        employee1.setId(1L);
        employee1.setFirstName("Charles");
        employee1.setSurname("Bukowski");
        employee1.setActive(true);
        employee1.setNote("mrtvy basnik");
        employee1.setCreated(LocalDateTime.now());

        AddressEntity address1 = new AddressEntity();
        address1.setStreetName("Lidická");
        address1.setZipCode("60200");
        address1.setCity("Brno");
        address1.setHouseNumber("12");
        address1.setCountry("CZE");
        employee1.setAddress(address1);

        EmployeeEntity employee2 = new EmployeeEntity();
        employee2.setId(2L);
        employee2.setFirstName("Jean");
        employee2.setSurname("Sibellius");
        employee2.setActive(true);
        employee2.setNote("mrtvy skladatel");
        employee2.setCreated(LocalDateTime.now());

        AddressEntity address2 = new AddressEntity();
        address2.setStreetName("Farska");
        address2.setZipCode("01898");
        address2.setCity("Stropkov");
        address2.setHouseNumber("1/a");
        address2.setCountry("SVK");
        employee2.setAddress(address2);

        DATA.add(employee1);
        DATA.add(employee2);
    }

    @Override
    public EmployeeEntity findOne(long id) {
        for (EmployeeEntity emp : DATA) {
            if (emp.getId().equals(id)) {
                return emp;
            }
        }
        return null;
    }

    @Override
    public List find(int offset, int limit) {
        return DATA.subList(offset, Math.min(offset + limit, DATA.size()));
    }

    @Override
    public void delete(long id) {
        for (EmployeeEntity emp : DATA) {
            if (emp.getId().equals(id)) {
                DATA.remove(emp);
                break;
            }
        }
    }

    @Override
    public EmployeeEntity update(EmployeeEntity entity) {
        for (EmployeeEntity emp : DATA) {
            if (emp.getId().equals(entity.getId())) {
                emp.setFirstName(entity.getFirstName());
                emp.setSurname(emp.getSurname());
                emp.setActive(true);
                emp.setAddress(emp.getAddress());
                return emp;
            }
        }
        return null;
    }

    @Override
    public EmployeeEntity create(EmployeeEntity entity) {
        EmployeeEntity employeeEntityLast = DATA.get(DATA.size() - 1);
        entity.setId(employeeEntityLast.getId() + 1);
        DATA.add(entity);
        return entity;
    }
}
