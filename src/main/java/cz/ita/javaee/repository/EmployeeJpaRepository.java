package cz.ita.javaee.repository;

import cz.ita.javaee.model.CompanyEntity;
import cz.ita.javaee.model.EmployeeEntity;
import cz.ita.javaee.repository.api.EmployeeRepository;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Repository
@Profile("jpa")
public class EmployeeJpaRepository implements EmployeeRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public EmployeeEntity findOne(long id) {
        return entityManager.find(EmployeeEntity.class, id);
    }

    @Override
    public List<EmployeeEntity> find(int offset, int limit) {
        Query findQuery = entityManager.createQuery("SELECT ee FROM EmployeeEntity AS ee", EmployeeEntity.class);
        findQuery.setFirstResult(offset);
        findQuery.setMaxResults(limit);
        return findQuery.getResultList();
    }

    @Override
    public void delete(long id) {
        EmployeeEntity employeeReference = entityManager.getReference(EmployeeEntity.class, id);
        entityManager.remove(employeeReference);
    }

    @Override
    public EmployeeEntity update(EmployeeEntity entity) {
        return entityManager.merge(entity);
    }

    @Override
    public EmployeeEntity create(EmployeeEntity entity) {
        entityManager.persist(entity);
        return entity;
    }
}