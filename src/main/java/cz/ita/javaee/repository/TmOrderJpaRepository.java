package cz.ita.javaee.repository;

import cz.ita.javaee.model.TmOrderEntity;
import cz.ita.javaee.repository.api.TmOrderRepository;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Repository
@Profile("jpa")
public class TmOrderJpaRepository implements TmOrderRepository {

    @PersistenceContext
    private EntityManager entityManager;

    public int getHighestId() {
        Query findQuery = entityManager.createNamedQuery("GET_ORDER_ID", TmOrderEntity.class);
        return findQuery.getFirstResult();
    }

    @Override
    public TmOrderEntity findOne(long id) {
        return entityManager.find(TmOrderEntity.class, id);
    }

    @Override
    public List<TmOrderEntity> find(int offset, int limit) {
        Query findQuery = entityManager.createNamedQuery("ORDER_TM_FIND_ALL", TmOrderEntity.class);
        findQuery.setFirstResult(offset);
        findQuery.setMaxResults(limit);
        return findQuery.getResultList();
    }

    @Override
    public void delete(long id) {
        TmOrderEntity reference = entityManager.getReference(TmOrderEntity.class, id);
        entityManager.remove(reference);
    }

    @Override
    public TmOrderEntity update(TmOrderEntity entity) {
        return entityManager.merge(entity);
    }

    @Override
    public TmOrderEntity create(TmOrderEntity entity) {
        entityManager.persist(entity);
        return entity;
    }
}
