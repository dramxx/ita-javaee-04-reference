package cz.ita.javaee.repository;

import cz.ita.javaee.dto.*;
import cz.ita.javaee.model.*;

import java.util.Set;
import java.util.stream.Collectors;

public class Transformations {

    public static CompanyEntity companyDtoToEntity(CompanyDto companyDto) {

        CompanyEntity companyEntity = new CompanyEntity();
        AddressEntity addressEntity = new AddressEntity();

        companyEntity.setId(companyDto.getId());
        companyEntity.setName(companyDto.getName());
        companyEntity.setCompanyId(companyDto.getCompanyId());
        companyEntity.setVatId(companyDto.getVatId());
        companyEntity.setActive(companyDto.isActive());
        companyEntity.setOwnershipped(companyDto.isOwnershipped());
        companyEntity.setCreated(companyDto.getCreated());

        addressEntity.setStreetName(companyDto.getAddress().getStreetName());
        addressEntity.setZipCode(companyDto.getAddress().getZipCode());
        addressEntity.setCity(companyDto.getAddress().getCity());
        addressEntity.setHouseNumber(companyDto.getAddress().getHouseNumber());
        addressEntity.setCountry(companyDto.getAddress().getCountry());
        companyEntity.setAddress(addressEntity);

        return companyEntity;
    }

    public static CompanyDto companyEntityToDto(CompanyEntity companyEntity) {

        CompanyDto companyDto = new CompanyDto();
        AddressDto addressDto = new AddressDto();

        companyDto.setId(companyEntity.getId());
        companyDto.setName(companyEntity.getName());
        companyDto.setCompanyId(companyEntity.getCompanyId());
        companyDto.setVatId(companyEntity.getVatId());
        companyDto.setActive(companyEntity.isActive());
        companyDto.setOwnershipped(companyEntity.isOwnershipped());
        companyDto.setCreated(companyEntity.getCreated());

        addressDto.setStreetName(companyEntity.getAddress().getStreetName());
        addressDto.setZipCode(companyEntity.getAddress().getZipCode());
        addressDto.setCity(companyEntity.getAddress().getCity());
        addressDto.setHouseNumber(companyEntity.getAddress().getHouseNumber());
        addressDto.setCountry(companyEntity.getAddress().getCountry());
        companyDto.setAddress(addressDto);

        return companyDto;
    }

    public static EmployeeEntity employeeDtoToEntity(EmployeeDto employeeDto) {

        EmployeeEntity employeeEntity = new EmployeeEntity();
        AddressEntity addressEntity = new AddressEntity();

        employeeEntity.setId(employeeDto.getId());
        employeeEntity.setFirstName(employeeDto.getFirstName());
        employeeEntity.setSurname(employeeDto.getSurname());
        employeeEntity.setActive(employeeDto.isActive());
        employeeEntity.setCreated(employeeDto.getCreated());

        addressEntity.setStreetName(employeeDto.getAddress().getStreetName());
        addressEntity.setZipCode(employeeDto.getAddress().getZipCode());
        addressEntity.setCity(employeeDto.getAddress().getCity());
        addressEntity.setHouseNumber(employeeDto.getAddress().getHouseNumber());
        addressEntity.setCountry(employeeDto.getAddress().getCountry());
        employeeEntity.setAddress(addressEntity);

        return employeeEntity;
    }

    public static EmployeeDto employeeEntityToDto(EmployeeEntity employeeEntity) {

        EmployeeDto employeeDto = new EmployeeDto();
        AddressDto addressDto = new AddressDto();

        employeeDto.setId(employeeEntity.getId());
        employeeDto.setFirstName(employeeEntity.getFirstName());
        employeeDto.setSurname(employeeEntity.getSurname());
        employeeDto.setActive(employeeEntity.isActive());
        employeeDto.setCreated(employeeEntity.getCreated());

        addressDto.setStreetName(employeeEntity.getAddress().getStreetName());
        addressDto.setZipCode(employeeEntity.getAddress().getZipCode());
        addressDto.setCity(employeeEntity.getAddress().getCity());
        addressDto.setHouseNumber(employeeEntity.getAddress().getHouseNumber());
        addressDto.setCountry(employeeEntity.getAddress().getCountry());
        employeeDto.setAddress(addressDto);

        return employeeDto;
    }

    public static TmDeliveryDto tmDeliveryEntityToDto(TmDeliveryEntity entity) {

        TmDeliveryDto tmDeliveryDto = new TmDeliveryDto();

        tmDeliveryDto.setPeriod(entity.getPeriod());
        tmDeliveryDto.setMdNumber(entity.getMdNumber());
        tmDeliveryDto.setOrder(tmOrderEntityToDto(entity.getOrder()));
        tmDeliveryDto.setId(entity.getId());
        tmDeliveryDto.setCreated(entity.getCreated());

        return tmDeliveryDto;
    }

    public static TmDeliveryEntity tmDeliveryDtoToEntity(TmDeliveryDto dto) {

        TmDeliveryEntity tmDeliveryEntity = new TmDeliveryEntity();

        tmDeliveryEntity.setPeriod(dto.getPeriod());
        tmDeliveryEntity.setMdNumber(dto.getMdNumber());
        tmDeliveryEntity.setOrder(tmOrderDtoToEntity(dto.getOrder()));
        tmDeliveryEntity.setId(dto.getId());
        tmDeliveryEntity.setCreated(dto.getCreated());

        return tmDeliveryEntity;
    }

    public static TmOrderDto tmOrderEntityToDto(TmOrderEntity entity) {

        TmOrderDto tmOrderDto = new TmOrderDto();

        tmOrderDto.setId(entity.getId());
        tmOrderDto.setNumber(entity.getNumber());
        tmOrderDto.setCreated(entity.getCreated());
        tmOrderDto.setProject(tmProjectEntityToDto(entity.getProject()));
        tmOrderDto.setType(entity.getType());
        tmOrderDto.setSupplier(companyEntityToDto(entity.getSupplier()));
        tmOrderDto.setSubscriber(companyEntityToDto(entity.getSubscriber()));
        tmOrderDto.setValidFrom(entity.getValidFrom());
        tmOrderDto.setValidTo(entity.getValidTo());
        tmOrderDto.setMdNumber(entity.getMdNumber());
        tmOrderDto.setMdRate(entity.getMdRate());
        tmOrderDto.setCurrency(entity.getCurrency());
        tmOrderDto.setMainOrder(tmOrderEntityToDto(entity.getMainOrder()));

        Set<TmOrderDto> orders = entity.getSubOrders()
                .stream()
                .map(o -> tmOrderEntityToDto(o))
                .collect(Collectors.toSet());
        tmOrderDto.setSubOrders(orders);

        Set<TmDeliveryDto> deliveries = entity.getDeliveries()
                .stream()
                .map(d -> tmDeliveryEntityToDto(d))
                .collect(Collectors.toSet());
        tmOrderDto.setDeliveries(deliveries);

        return tmOrderDto;
    }

    public static TmOrderEntity tmOrderDtoToEntity(TmOrderDto dto) {

        TmOrderEntity tmOrderEntity = new TmOrderEntity();

        tmOrderEntity.setId(dto.getId());
        tmOrderEntity.setNumber(dto.getNumber());
        tmOrderEntity.setCreated(dto.getCreated());
        tmOrderEntity.setProject(tmProjectDtoToEntity(dto.getProject()));
        tmOrderEntity.setType(dto.getType());
        tmOrderEntity.setSupplier(companyDtoToEntity(dto.getSupplier()));
        tmOrderEntity.setSubscriber(companyDtoToEntity(dto.getSubscriber()));
        tmOrderEntity.setValidFrom(dto.getValidFrom());
        tmOrderEntity.setValidTo(dto.getValidTo());
        tmOrderEntity.setMdNumber(dto.getMdNumber());
        tmOrderEntity.setMdRate(dto.getMdRate());
        tmOrderEntity.setCurrency(dto.getCurrency());
        tmOrderEntity.setMainOrder(tmOrderDtoToEntity(dto.getMainOrder()));

        Set<TmOrderEntity> orders = dto.getSubOrders()
                .stream()
                .map(o -> tmOrderDtoToEntity(o))
                .collect(Collectors.toSet());
        tmOrderEntity.setSubOrders(orders);

        Set<TmDeliveryEntity> deliveries = dto.getDeliveries()
                .stream()
                .map(d -> tmDeliveryDtoToEntity(d))
                .collect(Collectors.toSet());
        tmOrderEntity.setDeliveries(deliveries);

        return tmOrderEntity;
    }

    public static TmProjectDto tmProjectEntityToDto(TmProjectEntity entity) {

        TmProjectDto tmProjectDto = new TmProjectDto();

        Set<TmOrderDto> orders = entity.getOrders()
                .stream()
                .map(o -> tmOrderEntityToDto(o))
                .collect(Collectors.toSet());
        tmProjectDto.setOrders(orders);

        tmProjectDto.setCreated(entity.getCreated());
        tmProjectDto.setCustomer(companyEntityToDto(entity.getCustomer()));
        tmProjectDto.setId(entity.getId());
        tmProjectDto.setName(entity.getName());
        tmProjectDto.setNote(entity.getNote());
        tmProjectDto.setStatus(entity.getStatus());

        return tmProjectDto;
    }

    public static TmProjectEntity tmProjectDtoToEntity(TmProjectDto dto) {

        TmProjectEntity tmProjectEntity = new TmProjectEntity();

        Set<TmOrderEntity> orders = dto.getOrders()
                .stream()
                .map(o -> tmOrderDtoToEntity(o))
                .collect(Collectors.toSet());
        tmProjectEntity.setOrders(orders);

        tmProjectEntity.setCreated(dto.getCreated());
        tmProjectEntity.setCustomer(companyDtoToEntity(dto.getCustomer()));
        tmProjectEntity.setId(dto.getId());
        tmProjectEntity.setName(dto.getName());
        tmProjectEntity.setNote(dto.getNote());
        tmProjectEntity.setStatus(dto.getStatus());

        return tmProjectEntity;
    }
}