package cz.ita.javaee.repository;

import cz.ita.javaee.model.CompanyEntity;
import cz.ita.javaee.repository.api.CompanyRepository;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

@Repository
@Profile("jpa")
public class CompanyJpaRepository implements CompanyRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public CompanyEntity findOne(long id) {
        return entityManager.find(CompanyEntity.class, id);
    }

    @Override
    public List<CompanyEntity> find(int offset, int limit) {
        TypedQuery<CompanyEntity> findQuery = entityManager.createQuery("SELECT c FROM CompanyEntity as c", CompanyEntity.class);
        findQuery.setFirstResult(0);
        findQuery.setMaxResults(limit);
        return findQuery.getResultList();
    }

    @Override
    public void delete(long id) {
        CompanyEntity reference = entityManager.getReference(CompanyEntity.class, id);
        entityManager.remove(reference);
    }

    @Override
    public CompanyEntity update(CompanyEntity entity) {
        return entityManager.merge(entity);
    }

    @Override
    public CompanyEntity create(CompanyEntity entity) {
        entityManager.persist(entity);
        return entity;
    }
}
