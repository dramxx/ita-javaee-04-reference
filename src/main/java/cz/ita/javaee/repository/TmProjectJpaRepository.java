package cz.ita.javaee.repository;

import cz.ita.javaee.model.TmProjectEntity;
import cz.ita.javaee.repository.api.TmProjectRepository;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Repository
@Profile("jpa")
public class TmProjectJpaRepository implements TmProjectRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public TmProjectEntity findOne(long id) {
        return entityManager.find(TmProjectEntity.class, id);
    }

    @Override
    public List<TmProjectEntity> find(int offset, int limit) {
        Query findQuery = entityManager.createNamedQuery("PROJECT_TM_FIND_ALL_SELECT", TmProjectEntity.class);
        findQuery.setFirstResult(offset);
        findQuery.setMaxResults(limit);
        return findQuery.getResultList();
    }

    @Override
    public void delete(long id) {
        TmProjectEntity reference = entityManager.getReference(TmProjectEntity.class, id);
        entityManager.remove(reference);
    }

    @Override
    public TmProjectEntity update(TmProjectEntity entity) {
        return entityManager.merge(entity);
    }

    @Override
    public TmProjectEntity create(TmProjectEntity entity) {
        entityManager.persist(entity);
        return entity;
    }
}
