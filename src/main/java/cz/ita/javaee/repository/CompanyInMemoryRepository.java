package cz.ita.javaee.repository;

import cz.ita.javaee.model.AddressEntity;
import cz.ita.javaee.model.CompanyEntity;
import cz.ita.javaee.repository.api.CompanyRepository;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Repository
@Profile("inMemory")
public class CompanyInMemoryRepository implements CompanyRepository{

    private final List<CompanyEntity> DATA = new ArrayList<>();

    @PostConstruct
    private void init() {

        CompanyEntity company1 = new CompanyEntity();
        company1.setId(1L);
        company1.setName("Best soft s.r.o.");
        company1.setCompanyId("11223344");
        company1.setVatId("CY11223344");
        company1.setActive(true);
        company1.setOwnershipped(true);
        company1.setNote("Hlavní firma.");
        company1.setCreated(LocalDateTime.now());

        AddressEntity addressCompany1 = new AddressEntity();
        addressCompany1.setStreetName("Lidická");
        addressCompany1.setZipCode("60200");
        addressCompany1.setCountry("Brno");
        addressCompany1.setHouseNumber("12");
        addressCompany1.setCountry("CZE");
        company1.setAddress(addressCompany1);

        CompanyEntity company2 = new CompanyEntity();
        company2.setId(2L);
        company2.setName("Wekolo");
        company2.setCompanyId("11112222");
        company2.setVatId("SR11112222");
        company2.setActive(true);
        company2.setOwnershipped(false);
        company2.setCreated(LocalDateTime.now());

        AddressEntity addressCompany2 = new AddressEntity();
        addressCompany2.setStreetName("Farska");
        addressCompany2.setZipCode("01898");
        addressCompany2.setCountry("Stropkov");
        addressCompany2.setHouseNumber("1/a");
        addressCompany2.setCountry("SVK");
        company2.setAddress(addressCompany2);

        DATA.add(company1);
        DATA.add(company2);
    }

    @Override
    public CompanyEntity findOne(long id) {
        for (CompanyEntity comp : DATA) {
            if (comp.getId().equals(id)) {
                return comp;
            }
        }
        return null;
    }

    @Override
    public List<CompanyEntity> find(int offset, int limit) {
        return DATA.subList(offset, Math.min(offset + limit, DATA.size()));
    }

    @Override
    public void delete(long id) {
        for (CompanyEntity comp : DATA) {
            if (comp.getId().equals(id)) {
                DATA.remove(comp);
                break;
            }
        }
    }

    @Override
    public CompanyEntity update(CompanyEntity company) {
        for (CompanyEntity comp : DATA) {
            if (comp.getId().equals(company.getId())) {
                comp.setName(company.getName());
                comp.setCompanyId(company.getCompanyId());
                comp.setVatId(company.getVatId());
                comp.setActive(true);
                comp.setOwnershipped(false);
                comp.setAddress(company.getAddress());
                return comp;
            }
        }
        return null;
    }

    @Override
    public CompanyEntity create(CompanyEntity company) {
        CompanyEntity companyLast = DATA.get(DATA.size() - 1);
        company.setId(companyLast.getId() + 1);
        DATA.add(company);
        return company;
    }
}
