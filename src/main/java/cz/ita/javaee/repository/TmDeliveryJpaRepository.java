package cz.ita.javaee.repository;

import cz.ita.javaee.model.TmDeliveryEntity;
import cz.ita.javaee.repository.api.TmDeliveryRepository;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Repository
@Profile("jpa")
public class TmDeliveryJpaRepository implements TmDeliveryRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public TmDeliveryEntity findOne(long id) {
        return entityManager.find(TmDeliveryEntity.class, id);
    }

    @Override
    public List<TmDeliveryEntity> find(int offset, int limit) {
        Query findQuery = entityManager.createNamedQuery("DELIVERY_TM_FIND_ALL", TmDeliveryEntity.class);
        findQuery.setFirstResult(offset);
        findQuery.setMaxResults(limit);
        return findQuery.getResultList();
    }

    @Override
    public void delete(long id) {
        TmDeliveryEntity reference = entityManager.getReference(TmDeliveryEntity.class, id);
        entityManager.remove(reference);
    }

    @Override
    public TmDeliveryEntity update(TmDeliveryEntity entity) {
        return entityManager.merge(entity);
    }

    @Override
    public TmDeliveryEntity create(TmDeliveryEntity entity) {
        entityManager.persist(entity);
        return entity;
    }
}
