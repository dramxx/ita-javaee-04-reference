package cz.ita.javaee.type;

public enum RoleType {
    ADMIN,
    DEALER,
    SYSTEM_ADMIN
}
