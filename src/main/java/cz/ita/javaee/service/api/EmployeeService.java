package cz.ita.javaee.service.api;

import cz.ita.javaee.dto.EmployeeDto;

import java.util.List;

public interface EmployeeService {

    List<EmployeeDto> findAll();

    EmployeeDto create(final EmployeeDto company);

    EmployeeDto update(EmployeeDto company);

    void delete(Long id);
}
