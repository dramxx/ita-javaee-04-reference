package cz.ita.javaee.service.api;

import cz.ita.javaee.dto.TmProjectDto;

import java.util.List;

public interface TmProjectService {

    TmProjectDto find(Long id);

    List<TmProjectDto> findAll();

    TmProjectDto create(final TmProjectDto project);

    TmProjectDto update(TmProjectDto project);

    void delete(Long id);

}
