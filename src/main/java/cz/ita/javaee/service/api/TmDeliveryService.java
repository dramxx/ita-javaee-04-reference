package cz.ita.javaee.service.api;

import cz.ita.javaee.dto.TmDeliveryDto;

public interface TmDeliveryService {

    TmDeliveryDto create(final TmDeliveryDto delivery);

    TmDeliveryDto update(final TmDeliveryDto delivery);

    void delete(Long id);
}
