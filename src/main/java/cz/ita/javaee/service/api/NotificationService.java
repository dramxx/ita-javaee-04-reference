package cz.ita.javaee.service.api;

import org.springframework.stereotype.Service;

@Service
public interface NotificationService {
    void sendNotifiction(String title, String text);
}
