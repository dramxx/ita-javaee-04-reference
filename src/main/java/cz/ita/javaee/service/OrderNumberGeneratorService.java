package cz.ita.javaee.service;

import cz.ita.javaee.repository.TmOrderJpaRepository;
import cz.ita.javaee.service.api.OrderNumberFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class OrderNumberGeneratorService implements OrderNumberFactory {


    @Autowired
    private TmOrderJpaRepository tmOrderJpaRepository;

    @Override
    public String getOrderNumber() {
        int orderNumber = tmOrderJpaRepository.getHighestId() + 1;
        String resultNumber = String.format("%d%04d", LocalDateTime.now().getYear(), orderNumber);
        return resultNumber;
    }
}