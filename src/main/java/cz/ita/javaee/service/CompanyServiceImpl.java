package cz.ita.javaee.service;

import cz.ita.javaee.dto.CompanyDto;
import cz.ita.javaee.model.CompanyEntity;
import cz.ita.javaee.repository.Transformations;
import cz.ita.javaee.repository.api.CompanyRepository;
import cz.ita.javaee.service.api.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class CompanyServiceImpl implements CompanyService {

    @Autowired
    private CompanyRepository repository;
    @Autowired
    private NotificationEmailService notificationEmailService;

    @Override
    @Transactional(readOnly = true)
    public List<CompanyDto> findAll() {
        List<CompanyEntity> listEntities = repository.find(0, Integer.MAX_VALUE);
        List<CompanyDto> listDtos = new ArrayList<>();
        for (CompanyEntity entity : listEntities) {
            listDtos.add(Transformations.companyEntityToDto(entity));
        }
        return listDtos;
    }

    @Override
    @Transactional
    public CompanyDto create(CompanyDto company) {
        company.setActive(true);
        company.setCreated(LocalDateTime.now());
        repository.create(Transformations.companyDtoToEntity(company));
        notificationEmailService.sendNotifiction("Brainis Test msg: Company created", "test");
        return company;
    }

    @Override
    @Transactional
    public CompanyDto update(CompanyDto company) {
        repository.update(Transformations.companyDtoToEntity(company));
        return company;
    }

    @Override
    @Transactional
    public void delete(Long id) {
        repository.delete(id);
    }
}