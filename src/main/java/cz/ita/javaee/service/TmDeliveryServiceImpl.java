package cz.ita.javaee.service;

import cz.ita.javaee.dto.TmDeliveryDto;
import cz.ita.javaee.repository.Transformations;
import cz.ita.javaee.repository.api.TmDeliveryRepository;
import cz.ita.javaee.service.api.TmDeliveryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

@Service
public class TmDeliveryServiceImpl implements TmDeliveryService {


    @Autowired
    private TmDeliveryRepository tmDeliveryRepository;

    @Autowired
    private NotificationEmailService notificationEmailService;

    @Override
    @Transactional
    public TmDeliveryDto create(TmDeliveryDto delivery) {
        delivery.setCreated(LocalDateTime.now());
        tmDeliveryRepository.create(Transformations.tmDeliveryDtoToEntity(delivery));
        notificationEmailService.sendNotifiction("Brainis Test msg: Delivery created", "test");
        return delivery;
    }

    @Override
    @Transactional
    public TmDeliveryDto update(TmDeliveryDto delivery) {
        tmDeliveryRepository.update(Transformations.tmDeliveryDtoToEntity(delivery));
        return delivery;
    }

    @Override
    @Transactional
    public void delete(Long id) {
        tmDeliveryRepository.delete(id);
    }
}
