package cz.ita.javaee.service;

import cz.ita.javaee.dto.TmOrderDto;
import cz.ita.javaee.repository.Transformations;
import cz.ita.javaee.repository.api.TmOrderRepository;
import cz.ita.javaee.service.api.TmOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

@Service
public class TmOrderServiceImpl implements TmOrderService {

    @Autowired
    private TmOrderRepository tmOrderRepository;

    @Autowired
    private NotificationEmailService notificationEmailService;

    @Override
    @Transactional
    public TmOrderDto create(TmOrderDto order) {
        order.setCreated(LocalDateTime.now());
        tmOrderRepository.create(Transformations.tmOrderDtoToEntity(order));
        notificationEmailService.sendNotifiction("Brainis Test msg: Order created", "test");
        return order;
    }

    @Override
    @Transactional
    public TmOrderDto update(TmOrderDto order) {
        tmOrderRepository.update(Transformations.tmOrderDtoToEntity(order));
        return order;

    }

    @Override
    @Transactional
    public void delete(Long id) {
        tmOrderRepository.delete(id);
    }
}
