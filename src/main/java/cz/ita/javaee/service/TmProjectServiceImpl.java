package cz.ita.javaee.service;

import cz.ita.javaee.dto.TmProjectDto;
import cz.ita.javaee.repository.Transformations;
import cz.ita.javaee.repository.api.TmProjectRepository;
import cz.ita.javaee.service.api.TmProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TmProjectServiceImpl implements TmProjectService {

    @Autowired
    private TmProjectRepository tmProjectRepository;

    @Autowired
    private NotificationEmailService notificationEmailService;

    @Override
    @Transactional
    public TmProjectDto find(Long id) {
        TmProjectDto tpe = Transformations.tmProjectEntityToDto(tmProjectRepository.findOne(id));
        return tpe;
    }

    @Override
    @Transactional(readOnly = true)
    public List<TmProjectDto> findAll() {

        List<TmProjectDto> listDtos = tmProjectRepository.find(0, Integer.MAX_VALUE)
                .stream()
                .map(e -> Transformations.tmProjectEntityToDto(e))
                .collect(Collectors.toList());

        return listDtos;
    }

    @Override
    @Transactional
    public TmProjectDto create(TmProjectDto project) {
        project.setCreated(LocalDateTime.now());
        tmProjectRepository.create(Transformations.tmProjectDtoToEntity(project));
        notificationEmailService.sendNotifiction("Brainis Test msg: Project created", "test");
        return project;
    }

    @Override
    @Transactional
    public TmProjectDto update(TmProjectDto project) {
        tmProjectRepository.update(Transformations.tmProjectDtoToEntity(project));
        return project;
    }

    @Override
    @Transactional
    public void delete(Long id) {
        tmProjectRepository.delete(id);
    }
}
