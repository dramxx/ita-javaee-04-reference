package cz.ita.javaee.service;

import cz.ita.javaee.dto.EmployeeDto;
import cz.ita.javaee.model.EmployeeEntity;
import cz.ita.javaee.repository.Transformations;
import cz.ita.javaee.repository.api.EmployeeRepository;
import cz.ita.javaee.service.api.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    @Autowired
    private EmployeeRepository repository;

    @Autowired
    private NotificationEmailService notificationEmailService;

    @Override
    @Transactional(readOnly = true)
    public List<EmployeeDto> findAll() {
        List<EmployeeEntity> listEntities = repository.find(0, Integer.MAX_VALUE);
        List<EmployeeDto> listDtos = listEntities
                .stream()
                .map(e -> Transformations.employeeEntityToDto(e))
                .collect(Collectors.toList());
        return listDtos;
    }

    @Override
    @Transactional
    public EmployeeDto create(EmployeeDto employee) {
        employee.setActive(true);
        employee.setCreated(LocalDateTime.now());
        repository.create(Transformations.employeeDtoToEntity(employee));
        notificationEmailService.sendNotifiction("Brainis Test msg: Employee created", "test");
        return employee;
    }

    @Override
    @Transactional
    public EmployeeDto update(EmployeeDto employee) {
        repository.update(Transformations.employeeDtoToEntity(employee));
        return employee;
    }

    @Override
    @Transactional
    public void delete(Long id) {
        repository.delete(id);
    }
}
