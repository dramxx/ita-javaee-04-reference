package cz.ita.javaee.service;

import cz.ita.javaee.service.api.NotificationService;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
public class NotificationEmailService implements NotificationService {

    @Autowired
    private JavaMailSender mailSender;

    @Value("${mail.from}")
    private String mailFrom;

    @Value("${mail.to}")
    private String mailTo;

    @Override
    public void sendNotifiction(String title, String text) throws NoSuchBeanDefinitionException {
        SimpleMailMessage emailMessage = new SimpleMailMessage();
        emailMessage.setSubject(title);
        emailMessage.setText(text);
        emailMessage.setFrom(mailFrom);
        emailMessage.setTo(mailTo);
        mailSender.send(emailMessage);
    }
}
