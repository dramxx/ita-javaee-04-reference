package cz.ita.javaee.config;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@Configuration
@Aspect
@EnableAspectJAutoProxy(proxyTargetClass = true)
public class AspectLoggerConfiguration {

    private Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Around("execution(public * cz.ita.javaee.service..*.*(..))")
    public Object logFunctions(ProceedingJoinPoint joinPoint) throws Throwable {

        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        StringBuilder builder = new StringBuilder();

        int argsLength = joinPoint.getArgs().length;
        if (argsLength == 0) {
            builder.append("[none]");
        } else {
            for (int i = 0; i < argsLength; i++) {
                builder.append(signature.getParameterNames()[i]);
                builder.append(": ");
                builder.append(joinPoint.getArgs()[0]);
                if (argsLength > (i + 1)) builder.append(", ");
            }
        }

        LOGGER.info("Class: {} | Function: {} | Parameters: {}",
                joinPoint.getTarget().getClass().getName(),
                signature.getName(),
                builder.toString());

        Object proceed = joinPoint.proceed();

        LOGGER.debug("Class: {} | Function: {} | Return: {}",
                joinPoint.getTarget().getClass().getName(),
                signature.getName(),
                proceed);

        return proceed;
    }
}