package cz.ita.javaee.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.datasource.lookup.JndiDataSourceLookup;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@EnableTransactionManagement
public class DatabaseConfiguration {

    @Profile("!development")
    @Bean(name = "dataSource")
    public DataSource dataSourceJndi(@Value("${db.jndi}") String dbJndiName) {
        JndiDataSourceLookup lookup = new JndiDataSourceLookup();
        return lookup.getDataSource(dbJndiName);
    }

    @Profile("development")
    @Bean(name = "dataSource")
    public DataSource dataSourceDevelopment(@Value("${development.db.driver}") String dbDriver,
            @Value("${development.db.url}") String dbUrl,
            @Value("${development.db.username}") String dbUsername,
            @Value("${development.db.password}") String dbPassword) {
        DriverManagerDataSource driverManagerDataSource = new DriverManagerDataSource(dbUrl);
        driverManagerDataSource.setDriverClassName(dbDriver);
        driverManagerDataSource.setUsername(dbUsername);
        driverManagerDataSource.setPassword(dbPassword);
        return driverManagerDataSource;
    }

}
