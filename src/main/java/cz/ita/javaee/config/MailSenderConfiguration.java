package cz.ita.javaee.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import java.util.Properties;

@Configuration
public class MailSenderConfiguration {

    @Bean
    public PropertyPlaceholderConfigurer propertyMailConfigurer() {
        PropertyPlaceholderConfigurer propertyPlaceholderConfigurer = new PropertyPlaceholderConfigurer();
        propertyPlaceholderConfigurer.setLocation(new ClassPathResource("mail.properties"));
        propertyPlaceholderConfigurer.setIgnoreUnresolvablePlaceholders(true);
        return propertyPlaceholderConfigurer;
    }

    @Bean
    public JavaMailSender mailSender(@Value("${mail.username}") String username,
                                     @Value("${mail.host}") String host,
                                     @Value("${mail.port}") int port,
                                     @Value("${mail.password}") String password,
                                     @Value("${mail.protocol}") String protocol,
                                     @Value("${mail.smtp.starttls.enable}") boolean tls,
                                     @Value("${mail.smtp.auth}") boolean auth)

    {
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();

        mailSender.setHost(host);
        mailSender.setPort(port);
        mailSender.setUsername(username);
        mailSender.setPassword(password);

        Properties mailProperties = new Properties();
        mailProperties.put("mail.transport.protocol", protocol);
        mailProperties.put("mail.smtp.starttls.enable", tls);
        mailProperties.put("mail.smtp.auth", auth);
        mailSender.setJavaMailProperties(mailProperties);

        return mailSender;
    }
}
