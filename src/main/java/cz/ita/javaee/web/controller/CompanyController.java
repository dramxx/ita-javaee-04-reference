package cz.ita.javaee.web.controller;

import cz.ita.javaee.dto.CompanyDto;
import cz.ita.javaee.service.api.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/company")
public class CompanyController {

    @Autowired
    private CompanyService companyService;

    @RequestMapping(path = "", method = RequestMethod.GET)
    public List<CompanyDto> findAll() {
        return companyService.findAll();
    }

    @RequestMapping(path = "", method = RequestMethod.POST)
    public void create(@RequestBody CompanyDto company) {
        companyService.create(company);
    }

    @RequestMapping(path = "", method = RequestMethod.PUT)
    public void update(@RequestBody CompanyDto company) {
        companyService.update(company);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable Long id) {
        companyService.delete(id);
    }
}
