package cz.ita.javaee.web.controller;

import cz.ita.javaee.dto.TmOrderDto;
import cz.ita.javaee.service.api.TmOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/order-tm")
public class TmOrderController {

    @Autowired
    private TmOrderService tmOrderService;

    @RequestMapping(path = "", method = RequestMethod.POST)
    public TmOrderDto create(@RequestBody TmOrderDto tmOrderDto) {
        return tmOrderService.create(tmOrderDto);
    }

    @RequestMapping(path = "", method = RequestMethod.PUT)
    public TmOrderDto update(@RequestBody TmOrderDto tmOrderDto) {
        return tmOrderService.update(tmOrderDto);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable Long id) {
        tmOrderService.delete(id);
    }
}

