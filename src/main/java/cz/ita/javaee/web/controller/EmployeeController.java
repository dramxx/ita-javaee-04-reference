package cz.ita.javaee.web.controller;

import cz.ita.javaee.dto.EmployeeDto;
import cz.ita.javaee.service.api.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/employee")
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    @RequestMapping(path="", method = RequestMethod.GET)
    public List<EmployeeDto> findAll() {
        return employeeService.findAll();
    }

    @RequestMapping(path="", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    public EmployeeDto create(@RequestBody EmployeeDto company) {
        return employeeService.create(company);
    }

    @RequestMapping(path="", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.PUT)
    public EmployeeDto update(@RequestBody EmployeeDto employeeDto) {
        return employeeService.update(employeeDto);
    }
    @RequestMapping(path="/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable Long id) {
        employeeService.delete(id);
    }
}
