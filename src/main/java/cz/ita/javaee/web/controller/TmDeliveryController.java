package cz.ita.javaee.web.controller;

import cz.ita.javaee.dto.TmDeliveryDto;
import cz.ita.javaee.service.api.TmDeliveryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/tm-delivery")
public class TmDeliveryController {

    @Autowired
    private TmDeliveryService tmDeliveryService;

    @RequestMapping(path = "", method = RequestMethod.POST)
    public TmDeliveryDto create(@RequestBody TmDeliveryDto tmDeliveryDto) {
        return tmDeliveryService.create(tmDeliveryDto);
    }

    @RequestMapping(path = "", method = RequestMethod.PUT)
    public TmDeliveryDto update(@RequestBody TmDeliveryDto tmDeliveryDto) {
        return tmDeliveryService.update(tmDeliveryDto);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable Long id) {
        tmDeliveryService.delete(id);
    }
}
