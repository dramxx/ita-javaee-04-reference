package cz.ita.javaee.web.controller;

import cz.ita.javaee.dto.TmProjectDto;
import cz.ita.javaee.service.api.TmProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/tm-project")
public class TmProjectController {

    @Autowired
    private TmProjectService tmProjectService;

    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    public TmProjectDto find(@PathVariable Long id) {
        return tmProjectService.find(id);
    }

    @RequestMapping(path = "", method = RequestMethod.GET)
    public List<TmProjectDto> findAll() {
        return tmProjectService.findAll();
    }

    @RequestMapping(path = "", method = RequestMethod.POST)
    public TmProjectDto create(@RequestBody TmProjectDto tmProjectDto) {
        return tmProjectService.create(tmProjectDto);
    }

    @RequestMapping(path = "", method = RequestMethod.PUT)
    public TmProjectDto update(@RequestBody TmProjectDto tmProjectDto) {
        return tmProjectService.update(tmProjectDto);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable Long id) {
        tmProjectService.delete(id);
    }
}


