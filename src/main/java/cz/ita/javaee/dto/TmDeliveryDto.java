package cz.ita.javaee.dto;

import java.time.LocalDate;

public class TmDeliveryDto extends AbstractDto{
    private LocalDate period;
    private int mdNumber;
    private TmOrderDto order;

    public LocalDate getPeriod() {
        return period;
    }

    public void setPeriod(LocalDate period) {
        this.period = period;
    }

    public int getMdNumber() {
        return mdNumber;
    }

    public void setMdNumber(int mdNumber) {
        this.mdNumber = mdNumber;
    }

    public TmOrderDto getOrder() {
        return order;
    }

    public void setOrder(TmOrderDto order) {
        this.order = order;
    }

    @Override
    public String toString() {
        return "TmDeliveryDto{" +
                "period=" + period +
                ", mdNumber=" + mdNumber +
                ", order=" + order +
                ", id=" + getId() +
                ", created=" + getCreated() +
                '}';
    }
}
