/**
 * Xura Management Station
 * <p/>
 * Copyright (c) 2010-2017 Xura
 * http://www.xura.com
 * All Rights Reserved.
 */
package cz.ita.javaee.dto;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public class CompanyDto extends SubjectDto {
    @NotNull
    @Length(max = 100)
    private String name;
    @NotBlank
    @Length(max = 8)
    private String companyId;
    @Length(max = 10)
    private String vatId;
    @Valid
    private AddressDto address;
    @NotNull
    private boolean ownershipped;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getVatId() {
        return vatId;
    }

    public void setVatId(String vatId) {
        this.vatId = vatId;
    }

    public AddressDto getAddress() {
        return address;
    }

    public void setAddress(AddressDto address) {
        this.address = address;
    }

    public boolean isOwnershipped() {
        return ownershipped;
    }

    public void setOwnershipped(boolean ownershipped) {
        this.ownershipped = ownershipped;
    }
}
