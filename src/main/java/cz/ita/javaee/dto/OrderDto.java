package cz.ita.javaee.dto;

public abstract class OrderDto extends AbstractDto {

    private String number;

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
}
