package cz.ita.javaee.model;

import org.hibernate.validator.constraints.Range;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "TM_DELIVERY")
@NamedQueries({
        @NamedQuery(
                name = "DELIVERY_TM_FIND_ONE",
                query = "SELECT d FROM TmDeliveryEntity d LEFT JOIN d.order WHERE d.id = :id"),
        @NamedQuery(
                name = "DELIVERY_TM_FIND_ALL",
                query = "SELECT d FROM TmDeliveryEntity d LEFT JOIN d.order"),
})
public class TmDeliveryEntity extends AbstractEntity {

    @Column(name = "PERIOD", nullable = false)
    private LocalDate period;

    @Column(name = "MD_NUMBER", nullable = false)
    @Range(min = 1)
    private int mdNumber;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ORDER_ID", nullable = false)
    private TmOrderEntity order;

    public LocalDate getPeriod() {
        return period;
    }

    public void setPeriod(LocalDate period) {
        this.period = period;
    }

    public int getMdNumber() {
        return mdNumber;
    }

    public void setMdNumber(int mdNumber) {
        this.mdNumber = mdNumber;
    }

    public TmOrderEntity getOrder() {
        return order;
    }

    public void setOrder(TmOrderEntity order) {
        this.order = order;
    }
}
