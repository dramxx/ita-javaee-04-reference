package cz.ita.javaee.model;

import javax.persistence.*;
import javax.validation.constraints.Size;

@Entity
@Table(name = "EMPLOYEE")
public class EmployeeEntity extends SubjectEntity {

    @Column(name = "FIRST_NAME", nullable = false)
    @Size(min = 3, max = 50)
    private String firstName;

    @Column(name = "SURNAME", nullable = false)
    @Size(min = 3, max = 50)
    private String surname;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(
                    name = "streetName",
                    column = @Column(name = "ADDRESS_STREET_NAME", length = 50, nullable = false)
            ),
            @AttributeOverride(
                    name = "houseNumber",
                    column = @Column(name = "ADDRESS_HOUSE_NUMBER", length = 10, nullable = false)
            ),
            @AttributeOverride(
                    name = "city",
                    column = @Column(name = "ADDRESS_CITY", length = 50, nullable = false)
            ),
            @AttributeOverride(
                    name = "zipCode",
                    column = @Column(name = "ADDRESS_ZIP_CODE", length = 10, nullable = false)
            ),
            @AttributeOverride(
                    name = "country",
                    column = @Column(name = "ADDRESS_COUNTRY", length = 3, nullable = false)
            )
    })
    private AddressEntity address;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public AddressEntity getAddress() {
        return address;
    }

    public void setAddress(AddressEntity address) {
        this.address = address;
    }
}
