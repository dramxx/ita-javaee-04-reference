package cz.ita.javaee.model;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.Size;

@MappedSuperclass
public class OrderEntity extends AbstractEntity {

    @Column(name = "NUMBER", length = 20, nullable = false)
    @Size(min = 1, max = 20)
    private String number;

    public void setNumber(String number) {
        this.number = number;
    }

    public String getNumber() {
        return number;
    }
}
