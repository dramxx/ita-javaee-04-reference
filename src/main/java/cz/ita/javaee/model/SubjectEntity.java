package cz.ita.javaee.model;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public class SubjectEntity extends AbstractEntity {

    @Column(name = "ACTIVE", nullable = false)
    private boolean active;

    @Column(name = "NOTE")
    private String note;

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
