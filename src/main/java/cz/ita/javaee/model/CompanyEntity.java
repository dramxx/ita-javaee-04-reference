package cz.ita.javaee.model;

import javax.persistence.*;
import javax.validation.constraints.Size;

@Entity
@Table(name = "COMPANY")
public class CompanyEntity extends SubjectEntity {

    @Column(name = "NAME", nullable = false)
    @Size(min = 3, max = 100)
    private String name;

    @Column(name = "COMPANY_ID", nullable = false, length = 8)
    private String companyId;

    @Column(name = "VAT_ID", length = 12)
    private String vatId;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(
                    name = "streetName",
                    column = @Column(name = "ADDRESS_STREET_NAME", length = 50, nullable = false)
            ),
            @AttributeOverride(
                    name = "houseNumber",
                    column = @Column(name = "ADDRESS_HOUSE_NUMBER", length = 10, nullable = false)
            ),
            @AttributeOverride(
                    name = "city",
                    column = @Column(name = "ADDRESS_CITY", length = 50, nullable = false)
            ),
            @AttributeOverride(
                    name = "zipCode",
                    column = @Column(name = "ADDRESS_ZIP_CODE", length = 10, nullable = false)
            ),
            @AttributeOverride(
                    name = "country",
                    column = @Column(name = "ADDRESS_COUNTRY", length = 3, nullable = false)
            )
    })
    private AddressEntity address;

    @Column(name = "OWNERSHIPPED", nullable = false)
    private boolean ownershipped;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getVatId() {
        return vatId;
    }

    public void setVatId(String vatId) {
        this.vatId = vatId;
    }

    public AddressEntity getAddress() {
        return address;
    }

    public void setAddress(AddressEntity address) {
        this.address = address;
    }

    public boolean isOwnershipped() {
        return ownershipped;
    }

    public void setOwnershipped(boolean ownershipped) {
        this.ownershipped = ownershipped;
    }
}
