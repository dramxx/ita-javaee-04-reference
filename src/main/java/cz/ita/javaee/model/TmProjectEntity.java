package cz.ita.javaee.model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "TM_PROJECT")
@NamedQueries({
        @NamedQuery(
                name = "PROJECT_TM_FIND_ONE_SELECT",
                query = "SELECT tmp FROM TmProjectEntity tmp LEFT JOIN FETCH tmp.customer AS C"),
        @NamedQuery(
                name = "PROJECT_TM_FIND_ALL_SELECT",
                query = "SELECT tmp FROM TmProjectEntity tmp LEFT JOIN FETCH tmp.customer")
})
@Inheritance(strategy = InheritanceType.JOINED)
public class TmProjectEntity extends ProjectEntity {

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "project")
    private Set<TmOrderEntity> orders = new HashSet<>();

    public Set<TmOrderEntity> getOrders() {
        return orders;
    }

    public void setOrders(Set<TmOrderEntity> orders) {
        this.orders = orders;
    }
}
