package cz.ita.javaee.model;

import cz.ita.javaee.type.ProjectStatus;

import javax.persistence.*;
import javax.validation.constraints.Size;

@MappedSuperclass
public class ProjectEntity extends AbstractEntity {

    @Column(name = "NAME", length = 100, nullable = false)
    @Size(min = 3, max = 100)
    private String name;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CUSTOMER_ID")
    private CompanyEntity customer;

    //TODO: store int as string in pgsql
//    @Enumerated(EnumType.STRING)
    @Column(name = "PROJECT_STATUS")
    private ProjectStatus status = ProjectStatus.OPEN;

    @Column(name = "NOTE", length = 300)
    @Size(max = 300)
    private String note;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CompanyEntity getCustomer() {
        return customer;
    }

    public void setCustomer(CompanyEntity customer) {
        this.customer = customer;
    }

//    @Enumerated(EnumType.STRING)
    public ProjectStatus getStatus() {
        return status;
    }

    public void setStatus(ProjectStatus status) {
        this.status = status;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
