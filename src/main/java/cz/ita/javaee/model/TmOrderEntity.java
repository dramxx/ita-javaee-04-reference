package cz.ita.javaee.model;

import cz.ita.javaee.type.TmOrderType;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "TM_ORDER")
@NamedQueries({
        @NamedQuery(
                name = "GET_ORDER_ID",
                query = "SELECT MAX(tmo.id) FROM TmOrderEntity tmo"
        ),
        @NamedQuery(
                name = "ORDER_TM_FIND_ONE",
                query = "SELECT tmo FROM TmOrderEntity tmo " +
                        "LEFT JOIN FETCH tmo.deliveries " +
                        "LEFT JOIN FETCH tmo.mainOrder " +
                        "LEFT JOIN FETCH tmo.project " +
                        "LEFT JOIN FETCH tmo.subOrders " +
                        "LEFT JOIN FETCH tmo.subscriber " +
                        "LEFT JOIN FETCH tmo.supplier " +
                        "WHERE tmo.id = :pId"),
        @NamedQuery(
                name = "ORDER_TM_FIND_ALL",
                query = "SELECT tmo FROM TmOrderEntity tmo " +
                        "LEFT JOIN FETCH tmo.deliveries " +
                        "LEFT JOIN FETCH tmo.mainOrder " +
                        "LEFT JOIN FETCH tmo.project " +
                        "LEFT JOIN FETCH tmo.subOrders " +
                        "LEFT JOIN FETCH tmo.subscriber " +
                        "LEFT JOIN FETCH tmo.supplier ")
})
@Inheritance(strategy = InheritanceType.JOINED)
public class TmOrderEntity extends OrderEntity {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PROJECT_ID")
    private TmProjectEntity project;

    @Column(name = "ORDER_TYPE", nullable = false)
    private TmOrderType type;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SUPPLIER_ID", nullable = false)
    private CompanyEntity supplier;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SUBSCRIBER_ID", nullable = false)
    private CompanyEntity subscriber;

    @Column(name = "VALID_FROM", nullable = false)
    private LocalDate validFrom;

    @Column(name = "VALID_TO", nullable = false)
    private LocalDate validTo;

    @Column(name = "NUMBER_MD", nullable = false)
    @Size(min = 1)
    private int mdNumber;

    @Column(name = "RATE_MD", nullable = false)
    @Size(min = 1)
    private int mdRate;

    @Column(name = "CURRENCY", nullable = false)
    private String currency;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MAIN_ORDER_ID")
    private TmOrderEntity mainOrder;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "id")
    private Set<TmOrderEntity> subOrders = new HashSet<>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "order")
    private Set<TmDeliveryEntity> deliveries = new HashSet<>();

    public TmProjectEntity getProject() {
        return project;
    }

    public void setProject(TmProjectEntity project) {
        this.project = project;
    }

    public TmOrderType getType() {
        return type;
    }

    public void setType(TmOrderType type) {
        this.type = type;
    }

    public CompanyEntity getSupplier() {
        return supplier;
    }

    public void setSupplier(CompanyEntity supplier) {
        this.supplier = supplier;
    }

    public CompanyEntity getSubscriber() {
        return subscriber;
    }

    public void setSubscriber(CompanyEntity subscriber) {
        this.subscriber = subscriber;
    }

    public LocalDate getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(LocalDate validFrom) {
        this.validFrom = validFrom;
    }

    public LocalDate getValidTo() {
        return validTo;
    }

    public void setValidTo(LocalDate validTo) {
        this.validTo = validTo;
    }

    public int getMdNumber() {
        return mdNumber;
    }

    public void setMdNumber(int mdNumber) {
        this.mdNumber = mdNumber;
    }

    public int getMdRate() {
        return mdRate;
    }

    public void setMdRate(int mdRate) {
        this.mdRate = mdRate;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public TmOrderEntity getMainOrder() {
        return mainOrder;
    }

    public void setMainOrder(TmOrderEntity mainOrder) {
        this.mainOrder = mainOrder;
    }

    public Set<TmOrderEntity> getSubOrders() {
        return subOrders;
    }

    public void setSubOrders(Set<TmOrderEntity> subOrders) {
        this.subOrders = subOrders;
    }

    public Set<TmDeliveryEntity> getDeliveries() {
        return deliveries;
    }

    public void setDeliveries(Set<TmDeliveryEntity> deliveries) {
        this.deliveries = deliveries;
    }
}
