import _ from 'lodash';
import moment from 'moment';

class Company {
  constructor(data) {
    _.merge(this, data);
    this.created = data.created ? moment(data.created) : null;
  }
}

export default Company;
