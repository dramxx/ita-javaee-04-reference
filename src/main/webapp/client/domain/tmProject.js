import _ from 'lodash';
import moment from 'moment';
import TmOrder from "./tmOrder";

class TmProject {
  constructor(data) {
    _.merge(this, data);
    this.orders = (this.orders || []).map(order => new TmOrder(order));
    this.created = data.created ? moment(data.created) : null;
  }

  get currency() {
    return _.isEmpty(this.orders) ? null : this.orders[0].currency;
  }

  get incomingOrders() {
    return this.ordersByType('INCOMING');
  }

  get deliveryOrders() {
    return this.ordersByType('DELIVERY');
  }

  get incomingOrdersSum() {
    return this.ordersSum('INCOMING');
  }

  get deliveryOrdersSum() {
    return this.ordersSum('DELIVERY');
  }

  get deliveriesSum() {
    return _(this.deliveryOrders)
      .flatMap('deliveries')
      .map(delivery => delivery.price)
      .sum();
  }

  get profitSum() {
    return _(this.deliveryOrders)
      .flatMap('deliveries')
      .map(delivery => delivery.profit)
      .sum();
  }

  ordersByType(type) {
    return _.filter(this.orders, {type: type});
  }

  ordersSum(type) {
    return _(this.orders)
      .filter({type: type})
      .map(order => order.mdRate * order.mdNumber)
      .sum() || 0;
  }
}

export default TmProject;
