import tmProjectResource from '../resources/tmProjectResource';
import _ from 'lodash';
import TmProject from "../domain/tmProject";

export default {
  newTmProject() {
    return new TmProject({});
  },
  async find (id) {
    const response = await tmProjectResource.get({id: id});
    if (response.ok) {
      return new TmProject(response.data);
    } else {
      return null;
    }
  },
  async findAll () {
    const response = await tmProjectResource.query();
    if (response.ok) {
      return response.data.map((tmProjectData) => new TmProject(tmProjectData));
    } else {
      return null;
    }
  },
  async create (tmProject) {
    return tmProjectResource.save({}, _.pickBy(tmProject));
  },
  async update (tmProject) {
    return tmProjectResource.update({}, _.pickBy(tmProject));
  },
  async delete(id) {
    return tmProjectResource.delete({id: id});
  }
}
