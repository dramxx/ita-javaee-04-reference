import tmOrderResource from '../resources/tmOrderResource';
import _ from 'lodash';
import TmOrder from "../domain/tmOrder";

export default {
  newTmOrder(data) {
    return new TmOrder(data);
  },
  async findAll () {
    const response = await tmOrderResource.query();
    if (response.ok) {
      return response.data.map((tmOrderData) => new TmOrder(tmOrderData));
    } else {
      return null;
    }
  },
  async create (tmOrder) {
    const response = await tmOrderResource.save({}, _.pickBy(tmOrder));
    if (response.ok) {
      return new TmOrder(response.data);
    } else {
      return null;
    }

  },
  async update (tmOrder) {
    const response = await tmOrderResource.update({}, _.pickBy(tmOrder));
    if (response.ok) {
      return new TmOrder(response.data);
    } else {
      return null;
    }
  },
  async delete(id) {
    return tmOrderResource.delete({id: id});
  }
}
