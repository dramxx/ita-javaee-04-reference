import tmDeliveryService from "../../../services/tmDeliveryService";

const actions = {
  async create ({ dispatch}, tmDelivery) {
    try {
      let newDelivery = await tmDeliveryService.create(tmDelivery);
      if (newDelivery) {
        await dispatch('tmProject/addDelivery', newDelivery, { root: true });
        return true;
      } else {
        return false;
      }
    } catch (ex) {
      console.error(ex);
    }
  },
  async update ({ dispatch }, tmDelivery) {
    try {
      let updatedDelivery = await tmDeliveryService.update(tmDelivery);
      if (updatedDelivery) {
        await dispatch('tmProject/updateDelivery', updatedDelivery, { root: true });
        return true;
      } else {
        return false;
      }
    } catch (ex) {
      console.error(ex);
    }
  },
  async delete ({dispatch, commit}, id) {
    let response = await tmDeliveryService.delete(id);
    if (response.ok) {
      await dispatch('tmProject/deleteDelivery', id, { root: true });
      return true;
    } else {
      return false;
    }
  }
};

export default actions
