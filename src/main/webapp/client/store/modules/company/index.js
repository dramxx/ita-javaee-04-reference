import actions from './companyActions';
import getters from './companyGetters';
import mutations from './companyMutations';

const state = {
  items: []
};

export default {
  namespaced: true,
  state,
  getters: getters,
  mutations: mutations,
  actions: actions
}
