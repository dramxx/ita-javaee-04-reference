'use strict';
import _ from 'lodash';

export default {
  customers: state => {
    return _.filter(state.items || [], {ownershipped: false});
  },
  ownershipped: (state) => {
    return _.filter(state.items || [], {ownershipped: true});
  }
}
